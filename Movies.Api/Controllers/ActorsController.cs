﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movies.Api.Responses;
using Movies.Core.DTOs;
using Movies.Core.Entities;
using Movies.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Movies.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ActorsController : ControllerBase
    {
        private readonly IActorService _actorService;
        private readonly IMapper _mapper;

        public ActorsController(IActorService actorService, IMapper mapper)
        {
            _actorService = actorService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var ienumerable = _actorService.GetAll();
            var dto = _mapper.Map<IEnumerable<ActorResponse>>(ienumerable);
            var response = new ApiResponse<IEnumerable<ActorResponse>>(dto);

            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var entity = await _actorService.GetActor(id);
            var dto = _mapper.Map<ActorResponse>(entity);
            var response = new ApiResponse<ActorResponse>(dto);

            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post(ActorRequest request)
        {

            var entity = _mapper.Map<Actors>(request);

            await _actorService.AddActor(entity);

            var dto = _mapper.Map<ActorResponse>(entity);
            var response = new ApiResponse<ActorResponse>(dto);

            return Ok(response);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, ActorRequest request)
        {
            var entity = _mapper.Map<Actors>(request);
            entity.Id = id;
            var result = await _actorService.UpdateActor(entity);
            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _actorService.DelActor(id);
            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }
    }
}
