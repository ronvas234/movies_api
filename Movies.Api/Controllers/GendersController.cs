﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movies.Api.Responses;
using Movies.Core.DTOs;
using Movies.Core.Entities;
using Movies.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Movies.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class GendersController : ControllerBase
    {
        private readonly IGenderService _genderService;
        private readonly IMapper _mapper;

        public GendersController(IGenderService genderService, IMapper mapper)
        {
            _genderService = genderService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var ienumerable = _genderService.GetAll();
            var dto = _mapper.Map<IEnumerable<GendersResponse>>(ienumerable);
            var response = new ApiResponse<IEnumerable<GendersResponse>>(dto);

            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var entity = await _genderService.GetGender(id);
            var dto = _mapper.Map<GendersResponse>(entity);
            var response = new ApiResponse<GendersResponse>(dto);

            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post(GendersRequest request)
        {

            var entity = _mapper.Map<Genders>(request);

            await _genderService.AddGender(entity);

            var dto = _mapper.Map<GendersResponse>(entity);
            var response = new ApiResponse<GendersResponse>(dto);

            return Ok(response);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, GendersRequest request)
        {
            var entity = _mapper.Map<Genders>(request);
            entity.Id = id;
            var result = await _genderService.UpdateGender(entity);
            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _genderService.DelGender(id);
            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }
    }
}
