﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movies.Api.Responses;
using Movies.Core.DTOs;
using Movies.Core.Entities;
using Movies.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Movies.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class BillboardsController : ControllerBase
    {
        private readonly IBillboardService _billboardService;
        private readonly IMapper _mapper;

        public BillboardsController(IBillboardService billboardService, IMapper mapper)
        {
            _billboardService = billboardService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var ienumerable = _billboardService.GetAll();
            var dto = _mapper.Map<IEnumerable<BillboardResponse>>(ienumerable);
            var response = new ApiResponse<IEnumerable<BillboardResponse>>(dto);

            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var entity = await _billboardService.GetBillboards(id);
            var dto = _mapper.Map<BillboardResponse>(entity);
            var response = new ApiResponse<BillboardResponse>(dto);

            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post(BillboardsRequest request)
        {

            var entity = _mapper.Map<Billboards>(request);

            await _billboardService.AddBillboards(entity);

            var dto = _mapper.Map<BillboardResponse>(entity);
            var response = new ApiResponse<BillboardResponse>(dto);

            return Ok(response);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, BillboardsRequest request)
        {
            var entity = _mapper.Map<Billboards>(request);
            entity.Id = id;
            var result = await _billboardService.UpdateBillboards(entity);
            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _billboardService.DelBillboards(id);
            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }
    }
}
