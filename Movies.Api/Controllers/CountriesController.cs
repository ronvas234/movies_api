﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movies.Api.Responses;
using Movies.Core.DTOs;
using Movies.Core.Entities;
using Movies.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Movies.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        private readonly ICountryService _countryService;
        private readonly IMapper _mapper;

        public CountriesController(ICountryService countryService, IMapper mapper)
        {
            _countryService = countryService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var ienumerable = _countryService.GetAll();
            var dto = _mapper.Map<IEnumerable<CountriesResponse>>(ienumerable);
            var response = new ApiResponse<IEnumerable<CountriesResponse>>(dto);

            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var entity = await _countryService.GetCountry(id);
            var dto = _mapper.Map<CountriesResponse>(entity);
            var response = new ApiResponse<CountriesResponse>(dto);

            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post(CountriesRequest request)
        {

            var entity = _mapper.Map<Countries>(request);

            await _countryService.AddCountry(entity);

            var dto = _mapper.Map<CountriesResponse>(entity);
            var response = new ApiResponse<CountriesResponse>(dto);

            return Ok(response);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, CountriesRequest request)
        {
            var entity = _mapper.Map<Countries>(request);
            entity.Id = id;
            var result = await _countryService.UpdateCountry(entity);
            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _countryService.DelCountry(id);
            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }
    }
}
