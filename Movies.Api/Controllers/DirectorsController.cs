﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movies.Api.Responses;
using Movies.Core.DTOs;
using Movies.Core.Entities;
using Movies.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Movies.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class DirectorsController : ControllerBase
    {
        private readonly IDirectorService _directorService;
        private readonly IMapper _mapper;

        public DirectorsController(IDirectorService directorService, IMapper mapper)
        {
            _directorService = directorService;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var ienumerable = _directorService.GetAll();
            var dto = _mapper.Map<IEnumerable<DirectorResponse>>(ienumerable);
            var response = new ApiResponse<IEnumerable<DirectorResponse>>(dto);

            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var entity = await _directorService.GetDirector(id);
            var dto = _mapper.Map<DirectorResponse>(entity);
            var response = new ApiResponse<DirectorResponse>(dto);

            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post(DirectorRequest request)
        {

            var entity = _mapper.Map<Directors>(request);

            await _directorService.AddDirector(entity);

            var dto = _mapper.Map<DirectorResponse>(entity);
            var response = new ApiResponse<DirectorResponse>(dto);

            return Ok(response);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, DirectorRequest request)
        {
            var entity = _mapper.Map<Directors>(request);
            entity.Id = id;
            var result = await _directorService.UpdateDirector(entity);
            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _directorService.DelDirector(id);
            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }
    }
}
