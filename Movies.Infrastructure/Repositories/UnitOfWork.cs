﻿using Movies.Core.Entities;
using Movies.Core.Interfaces;
using Movies.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private readonly IRepository<Actors> _actorRepository;
        private readonly IRepository<Directors> _directorRepository;
        private readonly IRepository<Billboards> _billboardRepository;
        private readonly IRepository<Genders> _genderRepository;
        private readonly IRepository<Countries> _countryRepository;

        public UnitOfWork(ApplicationDbContext contex)
        {
            _context = contex;
        }

        public IRepository<Actors> ActorRepository => _actorRepository ?? new BaseRepository<Actors>(_context);
        public IRepository<Billboards> BillboardsRepository => _billboardRepository ?? new BaseRepository<Billboards>(_context);
        public IRepository<Directors> DirectorsRepository => _directorRepository ?? new BaseRepository<Directors>(_context);
        public IRepository<Genders> GendersRepository => _genderRepository ?? new BaseRepository<Genders>(_context);
        public IRepository<Countries> CountriesRepository => _countryRepository ?? new BaseRepository<Countries>(_context);


        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
