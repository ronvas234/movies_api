﻿using AutoMapper;
using Movies.Core.DTOs;
using Movies.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Infrastructure.Mappings
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Actors, ActorRequest>().ReverseMap();
            CreateMap<ActorResponse, Actors>().ReverseMap();
            CreateMap<Billboards, BillboardsRequest>().ReverseMap();
            CreateMap<BillboardResponse, Billboards>().ReverseMap();
            CreateMap<Countries, CountriesRequest>().ReverseMap();
            CreateMap<CountriesResponse, Countries>().ReverseMap();
            CreateMap<Directors, DirectorRequest>().ReverseMap();
            CreateMap<DirectorResponse, Directors>().ReverseMap();
            CreateMap<Genders, GendersRequest>().ReverseMap();
            CreateMap<GendersResponse, Genders>().ReverseMap();
        }
    }
}
