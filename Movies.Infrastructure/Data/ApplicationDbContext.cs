﻿using Microsoft.EntityFrameworkCore;
using Movies.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Infrastructure.Data
{
   public class ApplicationDbContext:DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Directors> Directors { get; set; }
        public DbSet<Genders> Genders { get; set; }
        public DbSet<Countries> Countries { get; set; }
        public DbSet<Actors> Actors { get; set; }
        public DbSet<Billboards> Billboards { get; set; }
    }
}
