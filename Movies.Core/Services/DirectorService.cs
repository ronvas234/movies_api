﻿using Movies.Core.Entities;
using Movies.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.Services
{
    public class DirectorService:IDirectorService
    {
        private readonly IUnitOfWork _unitOfWork;
        public DirectorService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Directors> GetAll()
        {
            return _unitOfWork.DirectorsRepository.GetAll();
        }

        public async Task<Directors> GetDirector(int id)
        {
            return await _unitOfWork.DirectorsRepository.GetById(id);
        }
        public async Task AddDirector(Directors director)
        {
            await _unitOfWork.DirectorsRepository.Add(director);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> DelDirector(int id)
        {
            await _unitOfWork.DirectorsRepository.Delete(id);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

        public async Task<bool> UpdateDirector(Directors director)
        {
            _unitOfWork.DirectorsRepository.Update(director);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }
    }
}
