﻿using Movies.Core.Entities;
using Movies.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.Services
{
    public class ActorService : IActorService
    {
        private readonly IUnitOfWork _unitOfWork;
        public ActorService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Actors> GetAll()
        {
            return _unitOfWork.ActorRepository.GetAll();
        }

        public async Task<Actors> GetActor(int id)
        {
            return await _unitOfWork.ActorRepository.GetById(id);
        }
        public async Task AddActor(Actors actor)
        {
            await _unitOfWork.ActorRepository.Add(actor);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> DelActor(int id)
        {
            await _unitOfWork.ActorRepository.Delete(id);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

        public async Task<bool> UpdateActor(Actors actor)
        {
            _unitOfWork.ActorRepository.Update(actor);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }
    }
}
