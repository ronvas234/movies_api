﻿using Movies.Core.Entities;
using Movies.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.Services
{
   public class GenderService:IGenderService
    {
        private readonly IUnitOfWork _unitOfWork;
        public GenderService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Genders> GetAll()
        {
            return _unitOfWork.GendersRepository.GetAll();
        }

        public async Task<Genders> GetGender(int id)
        {
            return await _unitOfWork.GendersRepository.GetById(id);
        }
        public async Task AddGender(Genders gender)
        {
            await _unitOfWork.GendersRepository.Add(gender);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> DelGender(int id)
        {
            await _unitOfWork.GendersRepository.Delete(id);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

        public async Task<bool> UpdateGender(Genders gender)
        {
            _unitOfWork.GendersRepository.Update(gender);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }
    }
}
