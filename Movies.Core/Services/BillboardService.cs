﻿using Movies.Core.Entities;
using Movies.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.Services
{
    public class BillboardService: IBillboardService
    {
        private readonly IUnitOfWork _unitOfWork;
        public BillboardService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Billboards> GetAll()
        {
            return _unitOfWork.BillboardsRepository.GetAll();
        }

        public async Task<Billboards> GetBillboards(int id)
        {
            return await _unitOfWork.BillboardsRepository.GetById(id);
        }
        public async Task AddBillboards(Billboards billboard)
        {
            await _unitOfWork.BillboardsRepository.Add(billboard);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> DelBillboards(int id)
        {
            await _unitOfWork.BillboardsRepository.Delete(id);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

        public async Task<bool> UpdateBillboards(Billboards billboard)
        {
            _unitOfWork.BillboardsRepository.Update(billboard);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }
    }
}
