﻿using Movies.Core.Entities;
using Movies.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.Services
{
   public class CountryService:ICountryService
    {
        private readonly IUnitOfWork _unitOfWork;
        public CountryService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Countries> GetAll()
        {
            return _unitOfWork.CountriesRepository.GetAll();
        }

        public async Task<Countries> GetCountry(int id)
        {
            return await _unitOfWork.CountriesRepository.GetById(id);
        }
        public async Task AddCountry(Countries country)
        {
            await _unitOfWork.CountriesRepository.Add(country);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> DelCountry(int id)
        {
            await _unitOfWork.CountriesRepository.Delete(id);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

        public async Task<bool> UpdateCountry(Countries country)
        {
            _unitOfWork.CountriesRepository.Update(country);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }
    }
}
