﻿using Movies.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.Interfaces
{
   public interface IUnitOfWork : IDisposable
    {
        IRepository<Actors> ActorRepository { get; }
        IRepository<Billboards> BillboardsRepository { get; }
        IRepository<Countries> CountriesRepository { get; }
        IRepository<Directors> DirectorsRepository { get; }
        IRepository<Genders> GendersRepository { get; }

        void SaveChanges();
        Task SaveChangesAsync();
    }
}
