﻿using Movies.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.Interfaces
{
   public interface IActorService
    {
        IEnumerable<Actors> GetAll();
        Task<Actors> GetActor(int id);
        Task AddActor(Actors actor);
        Task<bool> UpdateActor(Actors actor);
        Task<bool> DelActor(int id);
    }
}
