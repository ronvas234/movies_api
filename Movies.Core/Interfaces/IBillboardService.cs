﻿using Movies.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.Interfaces
{
    public interface IBillboardService
    {
        IEnumerable<Billboards> GetAll();
        Task<Billboards> GetBillboards(int id);
        Task AddBillboards(Billboards billboards);
        Task<bool> UpdateBillboards(Billboards billboards);
        Task<bool> DelBillboards(int id);
    }
}
