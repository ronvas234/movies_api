﻿using Movies.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.Interfaces
{
   public interface IGenderService
    {
        IEnumerable<Genders> GetAll();
        Task<Genders> GetGender(int id);
        Task AddGender(Genders gender);
        Task<bool> UpdateGender(Genders gender);
        Task<bool> DelGender(int id);
    }
}
