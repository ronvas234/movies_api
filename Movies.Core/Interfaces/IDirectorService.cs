﻿using Movies.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.Interfaces
{
    public interface IDirectorService
    {
        IEnumerable<Directors> GetAll();
        Task<Directors> GetDirector(int id);
        Task AddDirector(Directors director);
        Task<bool> UpdateDirector(Directors director);
        Task<bool> DelDirector(int id);
    }
}
