﻿using Movies.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.Interfaces
{
   public interface ICountryService
    {
        IEnumerable<Countries> GetAll();
        Task<Countries> GetCountry(int id);
        Task AddCountry(Countries countries);
        Task<bool> UpdateCountry(Countries countries);
        Task<bool> DelCountry(int id);
    }
}
