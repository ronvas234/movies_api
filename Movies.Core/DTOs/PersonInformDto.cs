﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.DTOs
{
    public class PersonInformDto
    {
        [Required]
        [StringLength(50,ErrorMessage = "It must not exceed the allowed parameters")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "It must not exceed the allowed parameters")]
        public string LastName { get; set; }
        [Required]
        public int IdCountry { get; set; }
    }
}
