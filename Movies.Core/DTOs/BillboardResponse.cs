﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.DTOs
{
    public class BillboardResponse
    {
        public int Id { get; set; }
        public string Titles { get; set; }
        public string Review { get; set; }
        public string Images { get; set; }
        public string CodeTrailer { get; set; }
        public int IdGender { get; set; }
        public int IdCountry { get; set; }
        public int IdActor { get; set; }
        public int IdDirector { get; set; }
    }
}
