﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.DTOs
{
   public class DirectorsRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int IdCountry { get; set; }
    }
}
