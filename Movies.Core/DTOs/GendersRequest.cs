﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.DTOs
{
    public class GendersRequest
    {
        [Required]
        [StringLength(50, ErrorMessage = "It must not exceed the allowed parameters")]
        public string Name { get; set; }
    }
}
