﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.DTOs
{
    public class BillboardsRequest
    {
        [Required]
        [StringLength(50, ErrorMessage = "It must not exceed the allowed parameters")]
        public string Titles { get; set; }
        [Required]
        [StringLength(1000, ErrorMessage = "It must not exceed the allowed parameters")]
        public string Review { get; set; }
        [Required]
        [StringLength(250, ErrorMessage = "It must not exceed the allowed parameters")]
        public string Images { get; set; }
        [Required]
        [StringLength(30, ErrorMessage = "It must not exceed the allowed parameters")]
        public string CodeTrailer { get; set; }
        [Required]
        public int IdGender { get; set; }
        [Required]
        public int IdCountry { get; set; }
        [Required]
        public int IdActor { get; set; }
        [Required]
        public int IdDirector { get; set; }
    }
}
