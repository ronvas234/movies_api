﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.Entities
{
    public class Genders: BaseEntity
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [ForeignKey("IdGender")]
        public virtual ICollection<Billboards> Billboards { get; set; }
    }
}
