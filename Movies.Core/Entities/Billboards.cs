﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.Entities
{
    public class Billboards:BaseEntity
    {
        [Required]
        [StringLength(50)]
        public string Titles { get; set; }
        [Required]
        [StringLength(1000)]
        public string Review { get; set; }
        [Required]
        [StringLength(250)]
        public string Images { get; set; }
        [Required]
        [StringLength(30)]
        public string CodeTrailer { get; set; }
        [Required]
        public int IdCountry { get; set; }
        [Required]
        public int IdGender { get; set; }
        [Required]
        public int IdActor { get; set; }
        [Required]
        public int IdDirector { get; set; }
        public virtual Genders Genders { get; set; }
    }
}
