﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movies.Core.Entities
{
    public class Countries : BaseEntity
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [ForeignKey("IdCountry")]
        public virtual ICollection<Actors> Actors { get; set; }
        [ForeignKey("IdCountry")]
        public virtual ICollection<Directors> Directors { get; set; }
    }
}
